---
title: About me
subtitle: All the juicy info about me.
comments: false
---

Hey! My name is OverThrow and I am a Discord Bot developer.

I enjoy playing video games but I don't really play them much anymore, I am now getting back into technical stuff, like programming.

I also enjoy watching movies! Here are my top 5 favorites:
*  Iron Man 3
*  Avengers Endgame
*  Spider Man: Far From Home
*  Star Wars: The Last Jedi
*  Star Wars: The Return of the Jedi

Here are my top 3 favorite T.V. shows:
* Sponge Bob SquarePants
* The Simpsons
* Gravity Falls



My top 3 favorite games:
* Minecraft
* Super Mario Galaxy
* Subnautica


Maybe you can relate on some of these! Any ways if you would like to contact me you can on Discord at OverThrowDev#3338 and you could also E-Mail me at wstrongvi@hotmal.com (though I don't really check it anymore.)